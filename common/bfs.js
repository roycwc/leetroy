var bfs = (head)=>{
    if (head === null) return [];
    var arr = []

    var queue = [head];

    while(queue.length > 0){
        var current = queue.shift();
        arr.push(current === null ? null : current.val);
        if (current !== null && !(current.left == null && current.right == null)){
            queue.push(current.left)
            queue.push(current.right)
        }
    }

    return arr;
}

module.exports = { bfs }