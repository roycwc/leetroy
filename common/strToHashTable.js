var strToHashTable = (str) => {
    var dict = {}
    for (var i = 0; i < str.length; i++) {
        var value = str[i]
        if (dict[value] === undefined) {
            dict[value] = 1;
        } else {
            dict[value]++;
        }
    }
    return dict;
}

module.exports = { strToHashTable }