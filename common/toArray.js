let toArray = (head)=>{
    let arr = [];
    var current = head
    while(true){
        if (current === null) return arr;
        arr.push(current.val)
        current = current.next;
    }
}
module.exports = { toArray }