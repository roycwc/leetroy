let toLinkedList = (arr, pos) => {
    class ListNode {
        constructor(val){
            this.val = val;
            this.next = null;
        }
    }

    for(let i=0;i<arr.length;i++){
        arr[i] = new ListNode(arr[i])
    }
    for(let i=0;i<arr.length;i++){
        arr[i].next = i === arr.length - 1 ? (pos === -1 ? null : arr[pos]) : arr[i+1]
    }
    return arr[0]
}

module.exports = { toLinkedList }
