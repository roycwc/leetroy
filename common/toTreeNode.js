
const toTreeNode = (arr)=>{
    class TreeNode {
        constructor(val, left, right){
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    var treeNodes = [];
    for(let i=0;i<arr.length;i++){
        treeNodes[i] = arr[i] === null ? null : new TreeNode(arr[i], null, null);
    }
    var root = treeNodes.shift()
    var queue = [root];

    while(queue.length > 0){
        var current = queue.shift();
        if (treeNodes.length > 0){
            var left = treeNodes.shift();
            current.left = left
            if (left !== null){
                queue.push(left)
            }
        }
        if (treeNodes.length > 0){
            var right = treeNodes.shift();
            current.right = right
            if (right !== null){
                queue.push(right)
            }
        }
    }

    return root;
    
}

const toCompletedTreeNode = (arr)=>{
    class TreeNode {
        constructor(val, left, right){
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    var treeNodes = [];
    for(let i=0;i<arr.length;i++){
        treeNodes[i] = new TreeNode(arr[i], null, null);
    }
    for(let i=0;i<arr.length;i++){
        var left = 2 * i + 1
        var right = 2 * i + 2
        if (left >= arr.length || right >= arr.length) continue;
        treeNodes[i].left = treeNodes[left]
        treeNodes[i].right = treeNodes[right]
    }
    return treeNodes[0];
}
module.exports = { toTreeNode, toCompletedTreeNode }