# Top 150 Leetcode solutions with test cases

> https://leetcode.com/studyplan/top-interview-150/

# Install deps
```sh
npm ci
```

# Run a test case
Run leetcode #13

```sh
npm run test -- tests/13.spec.js
```