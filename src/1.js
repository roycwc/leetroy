/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
    var arrayToHashTable = (arr) => {
        var dict = {}
        for (var i = 0; i < arr.length; i++) {
            var value = `${arr[i]}`
            if (dict[value] === undefined) {
                dict[value] = [i];
            } else {
                dict[value].push(i);
            }
        }
        return dict;
    }

    var popFromHashTable = (dict, key) => {
        key = `${key}`;
        if (dict[key] === undefined) return [dict, -1];
        var popValue = dict[key].shift()
        if (dict[key].length === 0) {
            delete dict[key]
        }
        return [dict, popValue];
    }


    var dict = arrayToHashTable(nums);

    for(let i=0;i<nums.length;i++){
        var [newDict1, j] = popFromHashTable(dict, nums[i])
        var [newDict2, k] = popFromHashTable(newDict1, target - nums[i])
        if (k >= 0) return [i,k];
    }

    return [];
};

module.exports = { twoSum }