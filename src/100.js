/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */

var bfs = function(head){
    if (head === null || head === undefined) return [];
    var arr = [];
    var queue = [head];
    while(queue.length > 0){
        var current = queue.shift();
        arr.push(current.val);
        if (!current.left && !current.right){
            continue;
        }
        if (current.left){
            queue.push(current.left)
        }else{
            arr.push(null)
        }
        if (current.right){
            queue.push(current.right)
        }
    }
    return arr;
}

/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree2 = function (p, q) {
    var pArr = bfs(p);
    var qArr = bfs(q);
    if (pArr.length !== qArr.length) return false;
    for(var i=0;i<pArr.length;i++){
        if (pArr[i] !== qArr[i])return false;
    }
    return true;
};


var isSameTree = function (p, q) {
    if (!p && !q) return true;
    if (!p || !q) return false;
    return p.val === q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
};


module.exports = { isSameTree }