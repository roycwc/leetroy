/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */

var invertTree = function (root) {
    if (!root) return null;
    if (!root.left && !root.right) return root;
    var invertedLeft = invertTree(root.left);
    var invertedRight = invertTree(root.right);
    root.left = invertedRight;
    root.right = invertedLeft;
    return root;
};

var bfs = (head)=>{
    if (head === null || head === undefined) return [];
    var arr = [];
    var queue = [head];
    while(queue.length > 0){
        var current = queue.shift();
        arr.push(current === null ? null : current.val);
        if (current === null) continue;
        queue.push(current.left)
        queue.push(current.right)
    }
    return arr;
}


var isSymmetric = function (root) {
    var r1Arr = bfs(root);
    var r2Arr = bfs(invertTree(root));
    for(var i=0;i<r1Arr.length;i++){
        if (r1Arr[i] !== r2Arr[i]) return false;
    }
    return true;
};

module.exports = { isSymmetric }