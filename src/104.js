/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */


var maxDepth_recursive = function(root){
    if (root === null) return 0;
    var leftDepth = 0;
    var rightDepth = 0;
    if (!root.left && !root.right) return 1;
    if (root.left) leftDepth = maxDepth(root.left);
    if (root.right) rightDepth = maxDepth(root.right);
    return 1 + Math.max(leftDepth, rightDepth);
}

/**
 * @param {TreeNode} root
 * @return {number}
 */
var maxDepth_bfs = function(root) {
    var bfs = (head)=>{
        if (head === null) return [];
        var maxDepth = 0;
        var arr = []
        var queue = [head];

        while(queue.length > 0){
            for(var i=0;i<queue.length;i++){
                var current = queue.shift();
                arr.push(current === null ? null : current.val);
                if (current.left != null ) queue.push(current.left)
                if (current.right != null ) queue.push(current.right)
            }
            maxDepth++;
        }

        return [arr, maxDepth];
}
    var [arr, maxDepth] = bfs(root);
    return maxDepth;

};

var maxDepth = maxDepth_recursive;


module.exports = { maxDepth }