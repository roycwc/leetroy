/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} nums
 * @return {TreeNode}
 */

class TreeNode {
    constructor(val, left, right){
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

var sortedArrayToBST = function(nums) {
    if (nums.length === 0) return null;
    if (nums.length === 1) return new TreeNode(nums[0],null, null)
    var mid = Math.floor(nums.length / 2);
    var left =  sortedArrayToBST(nums.slice(0, mid))
    var right = sortedArrayToBST(nums.slice(mid+1, nums.length))
    return new TreeNode(nums[mid], left, right);
};

module.exports = { sortedArrayToBST }