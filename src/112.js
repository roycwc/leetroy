/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {boolean}
 */
var hasPathSum = function(root, targetSum) {

    if (!root) return false
    

    var traverse = function(r, t){
        if (r.left === null && r.right === null){
            return r.val === t;
        }
        var left = false;
        if (r.left){
            r.left.val += r.val;
            left = traverse(r.left, t)
        }
        var right = false;
        if (r.right){
            r.right.val += r.val;
            right = traverse(r.right, t)
        }

        return left || right;
    }

    return traverse(root, targetSum);
};

module.exports = { hasPathSum };