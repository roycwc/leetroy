/**
 * @param {number[]} prices
 * @return {number}
 */
var maxProfit = function (prices) {
    var currentMaxProfit = 0
    var i = 0;
    var j = 1;
    while (true) {
        if (j === prices.length) break;
        if (prices[j] <= prices[i]) {
            i = j
            j = i + 1
            continue;
        }
        var profit = prices[j] - prices[i];
        if (profit > currentMaxProfit){
            currentMaxProfit = profit;
        }
        j++;
    }
    return currentMaxProfit;
};

module.exports = { maxProfit }