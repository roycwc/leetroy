/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function(s) {
    var k = 0;
    var i = 0;
    while(true){
        if (i === s.length) break;
        if (i < s.length - 1){
            switch (`${s[i]}${s[i+1]}`){
                case 'IV':
                    k += 4
                    i += 2
                    continue;
                case 'IX':
                    k += 9
                    i += 2
                    continue;
                case 'XL':
                    k += 40
                    i += 2
                    continue;
                case 'XC':
                    k += 90
                    i += 2
                    continue;
                case 'CD':
                    k += 400
                    i += 2
                    continue;
                case 'CM':
                    k += 900
                    i += 2
                    continue;
            }
        }
        switch (s[i]){
            case 'I':
                k += 1
                i += 1
                continue;
            case 'V':
                k += 5
                i += 1
                continue;
            case 'X':
                k += 10
                i += 1
                continue;
            case 'L':
                k += 50
                i += 1
                continue;
            case 'C':
                k += 100
                i += 1
                continue;
            case 'D':
                k += 500
                i += 1
                continue;
            case 'M':
                k += 1000
                i += 1
                continue;
        }
    }
    return k;
    
};

module.exports = { romanToInt }