/**
 * @param {number[]} nums
 * @return {number}
 */
var singleNumber = function (nums) {
    var f = 0;
    for (var i = 0; i < nums.length; i++) {
        f = f ^ nums[i];
    }
    return f;
};

module.exports = { singleNumber }