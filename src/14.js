/**
 * @param {string[]} strs
 * @return {string}
 */
var longestCommonPrefix = function(strs) {
    var result = "";
    for(let i=0; i< strs[0].length;i++){
        var letter = strs[0][i];
        for(let j=1;j<strs.length;j++){
            try{
                if (strs[j][i] !== letter)  return result;
            }catch(_){
                return result;
            }
        }
        result = result + strs[0][i];
    }
    return result

};

module.exports = { longestCommonPrefix }