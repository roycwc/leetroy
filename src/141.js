/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 */
var hasCycle = function (head) {
    if (head === null) return false;
    let p1 = head;
    let p2 = head.next;
    while(true){
        if (p1 === null || p2 === null) return false;
        if (p1 === p2) return true
        if (p1.next === null || p2.next === null || p2.next.next === null) return false;
        p1 = p1.next
        p2 = p2.next.next
    }
    
};


module.exports = { hasCycle }