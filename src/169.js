/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function (nums) {
    var count = 0;
    var result = undefined;
    var i = 0;
    while(true){
        if (i === nums.length) break;
        if (count === 0){
            result = nums[i];
            count = 1;
            i++;
            continue;
        }
        if (result !== nums[i]){
            count--;
            i++;
            continue;
        }
        count++;
        i++;
    }
    return result;
};

var majorityElement2 = function (nums) {
    var sortedNums = nums.sort();
    return sortedNums[Math.ceil(nums.length / 2)-1]
};

module.exports = { majorityElement }