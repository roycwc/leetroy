/**
 * @param {number} n - a positive integer
 * @return {number} - a positive integer
 */
var reverseBits = function (n) {
    var toBitStr = function (num) {
        var str = "";
        while(num > 0){
            if (num % 2 === 0){
                str = "0" + str;
                num = num / 2;
            }else{
                str = "1"  + str;
                num = (num - 1) / 2;
            }
        }
        return str;
    };

    var padZero = function(str, n){
        var newStr = "";
        for(var i=0;i< n - str.length;i++){
            newStr += "0"
        }
        return newStr + str;
        
    }

    var bitStrToInt = function(str){
        var n = 0;
        for(var i=0;i<str.length;i++){
            if (str[i] === "1"){
                n += Math.pow(2, 31 - i)
            }
        }
        return n;
    }

    var bitStr = toBitStr(n);
    var bitStrPadded = padZero(bitStr, 32)
    var reveredBitStr = bitStrPadded.split('').reverse().join('');
    var i = bitStrToInt(reveredBitStr);
    return i;
};

module.exports = { reverseBits }