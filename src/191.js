/**
 * @param {number} n - a positive integer
 * @return {number}
 */
var hammingWeight = function(n) {
    var toBitStr = function (num) {
        var str = "";
        while(num > 0){
            if (num % 2 === 0){
                str = "0" + str;
                num = num / 2;
            }else{
                str = "1"  + str;
                num = (num - 1) / 2;
            }
        }
        return str;
    };

    var bitStr = toBitStr(n);

    var oneCounter = 0;
    for(var i=0;i<bitStr.length;i++){
        if (bitStr[i] === "1") {
            oneCounter++;
        }
    }
    return oneCounter;
};

module.exports = { hammingWeight }