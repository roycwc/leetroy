/**
 * @param {string} s
 * @return {boolean}
 */
var isValid = function(s) {
    var dict1 = {
        "(":")",
        "[":"]",
        "{":"}",
    }
    var dict2 = {
        ")":"(",
        "]":"[",
        "}":"{",
    }

    var stack = []


    for(let i=0;i<s.length;i++){
        if (dict1[s[i]] !== undefined){
            stack.push(s[i]);
            continue;
        }
        if (dict2[s[i]] !== undefined){
            if (stack.length === 0) return false;
            var expected = stack.pop();
            if (expected !== dict2[s[i]]) return false;
        }
    }
    return stack.length === 0;
};

module.exports = { isValid }