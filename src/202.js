/**
 * @param {number} n
 * @return {boolean}
 */
var isHappy = function (n) {
    var dict = {}

    while(true){
        var nStr = n.toString();
        var nSum = 0;
        for(let i=0;i<nStr.length;i++){
            var nStrI = parseInt(nStr[i])
            nSum += nStrI * nStrI
        }
        if (nSum === 1) return true;
        if (dict[nStr] === undefined) {
            dict[nStr] = true
        }else {
            return false;
        }
        n = nSum;
    }
};

module.exports = { isHappy };
