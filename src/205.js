/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isIsomorphic = function (s, t) {
    var isIsomorphicOneWay = function (s1, t1) {
        var dict = {}
        for(let i=0;i<s1.length;i++){
            if (dict[s1[i]] === undefined){
                dict[s1[i]] = t1[i]
            }else if (dict[s1[i]] !== t1[i]){
                return false;
            }
        }
        return true;
    }

    return isIsomorphicOneWay(s, t) && isIsomorphicOneWay(t, s);

};

module.exports = { isIsomorphic }