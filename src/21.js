/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} list1
 * @param {ListNode} list2
 * @return {ListNode}
 */
var mergeTwoLists = function(list1, list2) {
    var current = { val: null, next: null}
    var head = current;

    var p1 = list1 || null;
    var p2 = list2 || null;
    while(true){
        if (p1 === null && p2 === null) return head.next;
        if (p2 === null){ // choose p1
            current.next = p1;
            current = current.next;
            p1 = p1.next;
            continue;
        }
        if (p1 === null){ // choose p2
            current.next = p2;
            current = current.next;
            p2 = p2.next;
            continue;
        }
        if (p1.val < p2.val){ // choose p1
            current.next = p1;
            current = current.next;
            p1 = p1.next;
            continue;
        } 
        if (p1.val >= p2.val){  // choose p2
            current.next = p2;
            current = current.next;
            p2 = p2.next;
            continue;
        }

    }
};

module.exports = { mergeTwoLists }