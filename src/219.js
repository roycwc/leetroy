/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */
var containsNearbyDuplicate = function (nums, k) {
  if (nums.length === 1) return false;
  var dict = {};

  for (let i = 0; i < nums.length; i++) {
    var key = nums[i].toString();
    if (dict[key] === undefined) {
      dict[key] = [i];
    } else {
      dict[key].push(i);
    }
  }
  var dictEntries = Object.entries(dict);
  for (let i = 0; i < dictEntries.length; i++) {
    for (let j = 0; j < dictEntries[i][1].length; j++) {
      if (j > 0 && dictEntries[i][1][j] - dictEntries[i][1][j - 1] <= k) {
        return true;
      }
    }
  }
  return false;
};

module.exports = { containsNearbyDuplicate };
