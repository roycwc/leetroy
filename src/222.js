/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var countNodes = function(root) {

    if (!root) return 0;

    var dfs = function(root){
        if (!root.left && !root.right){
            return [root.val];
        }
        var arr = [root.val]
        if (root.left){
            arr = arr.concat(dfs(root.left))
        }
        if (root.right){
            arr = arr.concat(dfs(root.right))
        }
        return arr;
    }

    var nums = dfs(root);
    return nums.length;
};

module.exports = { countNodes }