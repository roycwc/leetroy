/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */
var invertTree = function (root) {
    if (!root) return null;
    if (!root.left && !root.right) return root;
    var invertedLeft = invertTree(root.left);
    var invertedRight = invertTree(root.right);
    root.left = invertedRight;
    root.right = invertedLeft;
    return root;
};

module.exports = { invertTree }