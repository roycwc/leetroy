/**
 * @param {number[]} nums
 * @return {string[]}
 */
var summaryRanges = function (nums) {
  var summary = [];
  var currentIndex = -1;
  for (let i = 0; i <= nums.length; i++) {
    if (currentIndex === -1) {
      currentIndex = i;
      continue;
    }
    if (i < nums.length && nums[i - 1] + 1 == nums[i]) continue;
    
    var range = i - currentIndex === 1 ? `${nums[i-1]}` : `${nums[currentIndex]}->${nums[i-1]}`
    summary.push(range)
    currentIndex = i;
  }
  return summary
};

module.exports = { summaryRanges };
