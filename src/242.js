/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isAnagram = function(s, t) {
    if (s.length !== t.length) return false
    var strToHashTable = (str)=>{
        var dict = {}
        for(var i=0;i<str.length;i++){
            var value = str[i]
            if (dict[value] === undefined){
                dict[value] = 1;
            }else{
                dict[value]++;
            }
        }
        return dict;
    }

    var sDict = strToHashTable(s)
    var tDict = strToHashTable(t)
    
    var sDictEntries = Object.entries(sDict);

    for(let i=0;i<sDictEntries.length;i++){
        var [key, value] = sDictEntries[i]
        if (tDict[key] === undefined) return false;
        if (tDict[key] !== value) return false;
    }

    return true;
};

module.exports = { isAnagram }