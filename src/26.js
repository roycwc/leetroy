/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
    var i = 0;
    var j = 0;
    var tmp = undefined;
    var c = 0;
    while(true){
        if (i === nums.length || j === nums.length) break;
        if (nums[j] !== tmp){
            tmp = nums[j]
            nums[i] = nums[j]
            i++
            j++
            continue;
        }
        j++
        c++
    }
    return nums.length - c;
};

module.exports = { removeDuplicates }