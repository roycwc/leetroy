/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement = function(nums, val) {
    var i = 0;
    var j = 0;
    var c = 0;
    while(true){
        if (i === nums.length || j === nums.length) break;
        if (nums[j] !== val){
            nums[i] = nums[j]
            i = i + 1
            j = j + 1
            continue;
        }
        j++
        c++
    }
    return nums.length - c
};

module.exports = { removeElement }