/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr = function(haystack, needle) {
    for(let i=0;i<haystack.length;i++){
        if (haystack[i] === needle[0]){
            var failed = false;
            for(let j=0;j<needle.length;j++){
                if (haystack[i+j] !== needle[j]) {
                    failed = true;
                    break;
                }
            }
            if (failed){
                continue;
            }else{
                return i;
            }
        }
    }
    return -1;
};

var strStr2 = function(haystack, needle) {
    return haystack.indexOf(needle)
};

module.exports = { strStr }