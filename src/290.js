/**
 * @param {string} pattern
 * @param {string} s
 * @return {boolean}
 */
var wordPattern = function (pattern, s) {
    var s = s.replaceAll('constructor', '_constructor');
    const splitStr = (str) => {
        const strPieces = []
        var strPiecesIndex = 0;
        for (let i = 0; i < str.length; i++) {
            if (str[i] === " ") {
                strPiecesIndex = strPieces[strPiecesIndex] === undefined ? strPiecesIndex : strPiecesIndex + 1
            }else{
                if (strPieces[strPiecesIndex] === undefined) {
                    strPieces[strPiecesIndex] = str[i]
                } else {
                    strPieces[strPiecesIndex] = strPieces[strPiecesIndex] + str[i]
                }
            }
        }
        return strPiecesIndex=== 0 ? [str] : strPieces;
    }

    const s1Pcs = splitStr(s);

    var wordPatternOneWay = (pattern1, sPcs) => {
        var dict = {}
    
        for (let i = 0; i < pattern1.length; i++) {
            if (dict[pattern1[i]] === undefined) {
                if (sPcs[i] === undefined) return false
                dict[pattern1[i]] = sPcs[i]
            } else if (dict[pattern1[i]] !== sPcs[i]) {
                return false
            }
        }
        return true;
    }

    return wordPatternOneWay(pattern, s1Pcs) && wordPatternOneWay(s1Pcs, pattern)
};

module.exports = { wordPattern }