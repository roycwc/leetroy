/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var searchInsert = function (nums, target) {
    var left = 0;
    var mid = Math.floor(nums.length / 2)
    var right = nums.length - 1
    while(true){
        if (nums[mid] === target) return mid;
        if (nums[mid] > target) {
            right = mid;
            mid = Math.floor((right - left + 1) / 2)
        }else{
            left = mid;
            mid = Math.floor((right - left + 1) / 2) + left
        }
        if (left === mid) {
            if (target < nums[mid]) return mid;
            if (target > nums[mid]) return right+1;
        }
        if (right === mid) {
            if (target > nums[mid]) return mid+1;
            if (target <= nums[left]) return left;
            if (target < nums[mid]) return mid;
        }
    }
    
    
};

module.exports = { searchInsert }