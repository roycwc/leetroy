/**
 * @param {string} ransomNote
 * @param {string} magazine
 * @return {boolean}
 */
var canConstruct = function(ransomNote, magazine) {
    var strToHashTable = (str)=>{
        var dict = {}
        for(var i=0;i<str.length;i++){
            var value = str[i]
            if (dict[value] === undefined){
                dict[value] = 1;
            }else{
                dict[value]++;
            }
        }
        return dict;
    }
    var ransomNoteDict = strToHashTable(ransomNote)
    var magazineDict = strToHashTable(magazine)

    var ransomNoteEntries = Object.entries(ransomNoteDict);
    for(let i=0;i<ransomNoteEntries.length;i++){
        var [key, value] = ransomNoteEntries[i]
        if (magazineDict[key] === undefined) return false;
        if (magazineDict[key] < value) return false;
    }
    return true;
};

module.exports = { canConstruct }