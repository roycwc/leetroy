/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */
var isSubsequence = function (s, t) {
    var i = 0;
    var j = 0;
    while(true){
        if (i === s.length) return true;
        if (j === t.length) return false;
        if (s[i] !== t[j]){
            j++;
            continue;
        }
        i++;
        j++;
    }
};

module.exports = { isSubsequence }