/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var getMinimumDifference = function(root) {
    
    var dfs = function(root){
        if (!root.left && !root.right){
            return [root.val]
        }
        var arr = []
        if (root.left){
            arr = arr.concat(dfs(root.left))
        }
        arr.push(root.val)
        if (root.right){
            arr = arr.concat(dfs(root.right))
        }
        return arr;
    }

    var arr2 = dfs(root)

    var arr3 = arr2.map((it,i,arr)=>{
        if (i === 0) {
            return null;
        }else{
            return it - arr[i-1];
        }
    }).filter(it=>it)

    return Math.min(...arr3);
};

module.exports = { getMinimumDifference }