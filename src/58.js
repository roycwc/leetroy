/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLastWord = function(s) {
    var n = s.length - 1;
    var i = -1;
    while(true){
        if (n === -1) {
            return i === -1 ? 0 : i
        }
        if (s[n] === ' ') {
            if (i >= 0) return i;
            n--;
        }else{
            if (i===-1){
                i = 1;
            }else{
                i++;
            }
            n--;
        }
    }
};

var lengthOfLastWord2 = function(s) {
    var pcs = s.trim().split(' ')
    return pcs[pcs.length - 1].length
};

module.exports = { lengthOfLastWord }