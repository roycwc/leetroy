/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var averageOfLevels = function(root) {
    var bfs = function(root){
        var currentLevel = 0
        var queue = [root, null];
        var levels = []
        while(queue.length){
            var current = queue.shift();
            if (current === null){
                if (!queue.length) break;
                queue.push(current)
                currentLevel++;
                continue;
            }
            levels[currentLevel] = levels[currentLevel] === undefined ? [current.val] : [...levels[currentLevel], current.val];
            if (current.left){
                queue.push(current.left)
            }
            if (current.right){
                queue.push(current.right)
            }
        }
        return levels
    }

    var levels = bfs(root);
    return levels.map(it=>{
        return it.reduce((acc,it,i)=>acc+it,0) / it.length
    });
};

module.exports = { averageOfLevels }