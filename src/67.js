/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 */
var addBinary = function(a, b) {
    var aRev = a.split("").reverse().join("");
    var bRev = b.split("").reverse().join("");

    var plus = function(c1,c2){
        
        if (c1 === "1" && c2 === "1") return ["0",true];
        if (c1 === "1" && c2 === "0") return ["1",false];
        if (c1 === "0" && c2 === "1") return ["1",false];
        return ["0",false];
    }

    var len = Math.max(aRev.length, bRev.length)
    var carry = false
    var finalAnswer = "";
    for(var i=0;i<len;i++){
        var [answer, currentCarry] = plus(
            aRev[i] === undefined ? "0" : aRev[i], 
            bRev[i] === undefined ? "0" : bRev[i]
        )
        if (carry && answer === "0") {
            answer = "1";
        }else if (carry && answer === "1") {
            currentCarry = true
            answer = "0";
        }
        carry = currentCarry;
        finalAnswer = `${finalAnswer}${answer}`;
    }
    if (carry){
        finalAnswer = `${finalAnswer}1`;
    }
    return finalAnswer.split("").reverse().join("");
};

module.exports = { addBinary }