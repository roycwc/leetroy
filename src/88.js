/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */

var merge = function(nums1, m, nums2, n) {
    var i1 = nums1.length - 1
    var i2 = m - 1
    var j = n - 1

    while(true){
        if (i1 < 0) break;
        if (nums1[i2] > nums2[j] ||　j < 0){
            nums1[i1] = nums1[i2]
            i2--
            i1--
            continue;
        }
        if (nums1[i2] <= nums2[j] || i2 < 0){
            nums1[i1] = nums2[j]
            j--
            i1--
            continue;
        }
        
    }

    return nums1;

};

module.exports = { merge }