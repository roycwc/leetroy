const { assert } = require('chai')
const { twoSum } = require('../src/1');

it('case 1', () => {
    var nums = [2,7,11,15]
    var target = 9
    var k = twoSum(nums, target)
    assert.deepEqual([0,1], k)
})

it('case 2', () => {
    var nums = [3,2,4]
    var target = 6
    var k = twoSum(nums, target)
    assert.deepEqual([1,2], k)
})

it('case 3', () => {
    var nums = [3,3]
    var target = 6
    var k = twoSum(nums, target)
    assert.deepEqual([0,1], k)
})