const { assert } = require('chai')
const { isSameTree } = require('../src/100.js')
const { toTreeNode } = require('../common/toTreeNode.js')

it('case 1', () => {
    var p = [1, 2, 3];
    var q = [1, 2, 3];
    var pTree = toTreeNode(p);
    var qTree = toTreeNode(q);
    var k = isSameTree(pTree, qTree)
    assert.equal(true, k)
})


it('case 2', () => {
    var p = [1, 2];
    var q = [1, null, 2];
    var pTree = toTreeNode(p);
    var qTree = toTreeNode(q);
    var k = isSameTree(pTree, qTree)
    assert.equal(false, k)
})


it('case 3', () => {
    var p = [1, 2, 1];
    var q = [1, 1, 2];
    var pTree = toTreeNode(p);
    var qTree = toTreeNode(q);
    var k = isSameTree(pTree, qTree)
    assert.equal(false, k)
})



it('case 4', () => {
    var p = [];
    var q = [0];
    var pTree = toTreeNode(p);
    var qTree = toTreeNode(q);
    var k = isSameTree(pTree, qTree)
    assert.equal(false, k)
})



it('case 5', () => {
    var p = [1, null, 1];
    var q = [1, null, 1];
    var pTree = toTreeNode(p);
    var qTree = toTreeNode(q);
    var k = isSameTree(pTree, qTree)
    assert.equal(true, k)
})

