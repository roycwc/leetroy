const { assert } = require('chai')
const { isSymmetric } = require('../src/101.js')
const { toTreeNode } = require('../common/toTreeNode.js')

it('case 1', () => {
    var root = [1,2,2,3,4,4,3];
    var head = toTreeNode(root);
    var k = isSymmetric(head)
    assert.deepEqual(true, k)
})

it('case 2', () => {
    var root = [1,2,2,null,3,null,3];
    var head = toTreeNode(root);
    var k = isSymmetric(head)
    assert.deepEqual(false, k)
})

it('case 3', () => {
    var root = [2,3,3,4,5,5,4,null,null,8,9,null,null,9,8];
    var head = toTreeNode(root);
    var k = isSymmetric(head)
    assert.deepEqual(false, k)
})
