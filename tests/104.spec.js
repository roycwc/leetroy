const { assert } = require('chai')
const { maxDepth } = require('../src/104.js')
const { toTreeNode } = require('../common/toTreeNode.js')

it('case 1', () => {
    var rootArr = [3,9,20,null,null,15,7];
    var root = toTreeNode(rootArr);
    var k = maxDepth(root)
    assert.equal(3, k)
})

it('case 2', () => {
    var rootArr = [1,null,2]
    var root = toTreeNode(rootArr);
    var k = maxDepth(root)
    assert.equal(2, k)
})

it('case 3', () => {
    var rootArr = [1,2,null,3,null,4,null,5]
    var root = toTreeNode(rootArr);
    var k = maxDepth(root)
    assert.equal(5, k)
})
