const { assert } = require('chai')
const { bfs } = require('../common/bfs.js')
const { sortedArrayToBST } = require('../src/108');

it('case 1', () => {
    var nums  = [-10,-3,0,5,9]
    var bst = sortedArrayToBST(nums)
    var result = bfs(bst)
    assert.deepEqual([0,-3,9,-10,null,5, null], result)
})

it('case 2', () => {
    var nums  = [1,3]
    var bst = sortedArrayToBST(nums)
    var result = bfs(bst)
    assert.deepEqual([3,1,null], result)
})