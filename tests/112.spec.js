const { assert } = require('chai')
const { hasPathSum } = require('../src/112.js')
const { toTreeNode } = require('../common/toTreeNode.js')

it('case 1', () => {
    var root = [5, 4, 8, 11, null, 13, 4, 7, 2, null, null, null, 1];
    var head = toTreeNode(root);
    var targetSum = 22;
    var k = hasPathSum(head, targetSum)
    assert.deepEqual(true, k)
})


it('case 2', () => {
    var root = [1,2,3];
    var head = toTreeNode(root);
    var targetSum = 5;
    var k = hasPathSum(head, targetSum)
    assert.deepEqual(false, k)
})


it('case 3', () => {
    var root = [];
    var head = toTreeNode(root);
    var targetSum = 0;
    var k = hasPathSum(head, targetSum)
    assert.deepEqual(false, k)
})