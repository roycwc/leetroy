const { assert } = require('chai')
const { isPalindrome } = require('../src/125');

it('case 1', () => {
    var s = "A man, a plan, a canal: Panama"
    var k = isPalindrome(s)
    assert.equal(true, k)
})

it('case 2', () => {
    var s = "race a car"
    var k = isPalindrome(s)
    assert.equal(false, k)
})

it('case 3', () => {
    var s = " "
    var k = isPalindrome(s)
    assert.equal(true, k)
})