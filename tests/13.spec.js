const { assert } = require('chai')
const { romanToInt } = require('../src/13');

it('case 1', () => {
    var s = "III"
    var k = romanToInt(s)
    assert.equal(3, k)
})

it('case 2', () => {
    var s = "LVIII"
    var k = romanToInt(s)
    assert.equal(58, k)
})

it('case 3', () => {
    var s = "MCMXCIV"
    var k = romanToInt(s)
    assert.equal(1994, k)
})


it('case 4', () => {
    var s = "MMMXLV"
    var k = romanToInt(s)
    assert.equal(3045, k)
})
