const { assert } = require('chai')
const { singleNumber } = require('../src/136');

it('case 1', () => {
    var s = [2,2,1]
    var k = singleNumber(s)
    assert.equal(1, k)
})
