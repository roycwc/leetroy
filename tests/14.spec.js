const { assert } = require('chai')
const { longestCommonPrefix } = require('../src/14');

it('case 1', () => {
    var s = ["flower","flow","flight"]
    var k = longestCommonPrefix(s)
    assert.equal("fl", k)
})

it('case 2', () => {
    var s = ["dog","racecar","car"]
    var k = longestCommonPrefix(s)
    assert.equal("", k)
})

it('case 3', () => {
    var s = [""]
    var k = longestCommonPrefix(s)
    assert.equal("", k)
})


