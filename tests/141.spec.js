const { assert } = require('chai')
const { hasCycle } = require('../src/141');
const { toLinkedList } = require('../common/toLinkedList');

it('case 1', () => {
    var arr  = [3,2,0,-4]
    var pos  = 1
    var linkedList = toLinkedList(arr, pos)
    var result = hasCycle(linkedList)
    assert.equal(true, result)
})


it('case 2', () => {
    var arr  = [1,2]
    var pos  = 0
    var linkedList = toLinkedList(arr, pos)
    var result = hasCycle(linkedList)
    assert.equal(true, result)
})


it('case 3', () => {
    var arr  = [1]
    var pos  = -1
    var linkedList = toLinkedList(arr, pos)
    var result = hasCycle(linkedList)
    assert.equal(false, result)
})
