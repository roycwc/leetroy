const { assert } = require('chai')
const { majorityElement } = require('../src/169');

it('case 1', () => {
    var nums = [3,2,3]
    var k = majorityElement(nums)
    assert.equal(3, k)
})

it('case 2', () => {
    var nums = [2,2,1,1,1,2,2]
    var k = majorityElement(nums)
    assert.equal(2, k)
})

it('case 2', () => {
    var nums = [4,4,3,4,99,8,4,33,4,22,8,3,4,4,3,4]
    var k = majorityElement(nums)
    assert.equal(4, k)
})