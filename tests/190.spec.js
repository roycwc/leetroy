const { assert } = require('chai')
const { reverseBits } = require('../src/190');

it('case 1', () => {
    var n  = "00000010100101000001111010011100"
    var b = parseInt(n, 2)
    var k = reverseBits(b)
    assert.equal(964176192, k)
})
