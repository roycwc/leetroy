const { assert } = require('chai')
const { hammingWeight } = require('../src/191');

it('case 1', () => {
    var a  = "00000000000000000000000000001011"
    var b  = parseInt(a, 2)
    var k = hammingWeight(b)
    assert.equal(3, k)
})
