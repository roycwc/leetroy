const { assert } = require('chai')
const { isValid } = require('../src/20');

it('case 1', () => {
    var s  = "()"
    var result = isValid(s)
    assert.equal(true, result)
})
it('case 2', () => {
    var s  = "()[]{}"
    var result = isValid(s)
    assert.equal(true, result)
})
it('case 3', () => {
    var s  = "(]"
    var result = isValid(s)
    assert.equal(false, result)
})
it('case 4', () => {
    var s  = "[{()}]"
    var result = isValid(s)
    assert.equal(true, result)
})
