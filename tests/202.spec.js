const { assert } = require('chai')
const { isHappy } = require('../src/202');

it('case 1', () => {
    var n = 19
    var k = isHappy(n)
    assert.equal(true, k)
})

it('case 2', () => {
    var n = 2
    var k = isHappy(n)
    assert.equal(false, k)
})
