const { assert } = require('chai')
const { isIsomorphic } = require('../src/205');

it('case 1', () => {
    var s = "egg"
    var t = "add"
    var k = isIsomorphic(s, t)
    assert.equal(true, k)
})

it('case 2', () => {
    var s = "foo"
    var t = "bar"
    var k = isIsomorphic(s, t)
    assert.equal(false, k)
})

it('case 3', () => {
    var s = "paper"
    var t = "title"
    var k = isIsomorphic(s, t)
    assert.equal(true, k)
})

it('case 4', () => {
    var s = "badc"
    var t = "baba"
    var k = isIsomorphic(s, t)
    assert.equal(false, k)
})