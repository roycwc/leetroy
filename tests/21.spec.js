const { assert } = require('chai')
const { mergeTwoLists } = require('../src/21');
const { toLinkedList } = require('../common/toLinkedList');
const { toArray } = require('../common/toArray');

it('case 1', () => {
    var list1 = [1, 2, 4]
    var list2 = [1, 3, 4]
    var linkedList1 = toLinkedList(list1, -1)
    var linkedList2 = toLinkedList(list2, -1)
    var linkedListResult = mergeTwoLists(linkedList1, linkedList2)
    var result = toArray(linkedListResult)
    assert.deepEqual([1, 1, 2, 3, 4, 4], result)
})

it('case 2', () => {
    var list1 = []
    var list2 = []
    var linkedList1 = toLinkedList(list1, -1)
    var linkedList2 = toLinkedList(list2, -1)
    var linkedListResult = mergeTwoLists(linkedList1, linkedList2)
    var result = toArray(linkedListResult)
    assert.deepEqual([], result)
})


it('case 3', () => {
    var list1 = []
    var list2 = [0]
    var linkedList1 = toLinkedList(list1, -1)
    var linkedList2 = toLinkedList(list2, -1)
    var linkedListResult = mergeTwoLists(linkedList1, linkedList2)
    var result = toArray(linkedListResult)
    assert.deepEqual([0], result)
})

it('case 4', () => {
    var list1 = [5]
    var list2 = [1,2,4]
    var linkedList1 = toLinkedList(list1, -1)
    var linkedList2 = toLinkedList(list2, -1)
    var linkedListResult = mergeTwoLists(linkedList1, linkedList2)
    var result = toArray(linkedListResult)
    assert.deepEqual([1,2,4,5], result)
})

it('case 5', () => {
    var list1 = [-9,-7,-3,-3,-1,2,3]
    var list2 = [-7,-7,-6,-6,-5,-3,2,4]
    var linkedList1 = toLinkedList(list1, -1)
    var linkedList2 = toLinkedList(list2, -1)
    var linkedListResult = mergeTwoLists(linkedList1, linkedList2)
    var result = toArray(linkedListResult)
    assert.deepEqual([-9,-7,-7,-7,-6,-6,-5,-3,-3,-3,-1,2,2,3,4], result)
})
