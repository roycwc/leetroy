const { assert } = require('chai')
const { containsNearbyDuplicate } = require('../src/219');

it('case 1', () => {
    var nums  = [1,2,3,1]
    var k  = 3
    var result = containsNearbyDuplicate(nums, k)
    assert.equal(true, result)
})

it('case 2', () => {
    var nums  = [1,0,1,1]
    var k  = 1
    var result = containsNearbyDuplicate(nums, k)
    assert.equal(true, result)
})

it('case 3', () => {
    var nums  = [1,2,3,1,2,3]
    var k  = 2
    var result = containsNearbyDuplicate(nums, k)
    assert.equal(false, result)
})

it('case 4', () => {
    var nums  = [1]
    var k  = 1
    var result = containsNearbyDuplicate(nums, k)
    assert.equal(false, result)
})

it('case 5', () => {
    var nums  = [-1,-1]
    var k  = 1
    var result = containsNearbyDuplicate(nums, k)
    assert.equal(true, result)
})
