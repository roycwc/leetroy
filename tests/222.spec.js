const { assert } = require('chai')
const { toTreeNode } = require('../common/toTreeNode.js')
const { countNodes } = require('../src/222');

it('case 1', () => {
    var nums  = [1,2,3,4,5,6]
    var root = toTreeNode(nums)
    var result = countNodes(root)
    assert.equal(6, result)
})


it('case 2', () => {
    var nums  = []
    var root = toTreeNode(nums)
    var result = countNodes(root)
    assert.equal(0, result)
})


it('case 3', () => {
    var nums  = [1]
    var root = toTreeNode(nums)
    var result = countNodes(root)
    assert.equal(1, result)
})