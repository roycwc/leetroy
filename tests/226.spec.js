const { assert } = require('chai')
const { invertTree } = require('../src/226.js')
const { toTreeNode } = require('../common/toTreeNode.js')
const { bfs } = require('../common/bfs.js')

it('case 1', () => {
    var root = [4,2,7,1,3,6,9];
    var head = toTreeNode(root);
    var invertedHead = invertTree(head)
    var k = bfs(invertedHead)
    assert.deepEqual([4,7,2,9,6,3,1], k)
})

it('case 2', () => {
    var root = [2,1,3];
    var head = toTreeNode(root);
    var invertedHead = invertTree(head)
    var k = bfs(invertedHead)
    assert.deepEqual([2,3,1], k)
})

it('case 3', () => {
    var root = [];
    var head = toTreeNode(root);
    var invertedHead = invertTree(head)
    var k = bfs(invertedHead)
    assert.deepEqual([], k)
})
