const { assert } = require('chai')
const { summaryRanges } = require('../src/228');

it('case 1', () => {
    var nums  = [0,1,2,4,5,7]
    var result = summaryRanges(nums)
    assert.deepEqual(["0->2","4->5","7"], result)
})

it('case 2', () => {
    var nums  = [0,2,3,4,6,8,9]
    var result = summaryRanges(nums)
    assert.deepEqual(["0","2->4","6","8->9"], result)
})
