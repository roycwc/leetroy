const { assert } = require('chai')
const { isAnagram } = require('../src/242');

it('case 1', () => {
    var s = "anagram"
    var t = "nagaram"
    var k = isAnagram(s, t)
    assert.equal(true, k)
})

it('case 2', () => {
    var s = "rat"
    var t = "car"
    var k = isAnagram(s, t)
    assert.equal(false, k)
})

it('case 3', () => {
    var s = "a"
    var t = "ab"
    var k = isAnagram(s, t)
    assert.equal(false, k)
})
