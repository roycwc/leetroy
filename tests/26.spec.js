const { assert } = require('chai')
const { removeDuplicates } = require('../src/26');

it('case 1', () => {
    var expectedNums = [1,2]
    var nums = [1,1,2]
    var k = removeDuplicates(nums)
    assert.equal(expectedNums.length, k)
    for (let i = 0; i < k; i++) {
        assert.equal(expectedNums[i], nums[i]);
    }
})

it('case 2', () => {
    var expectedNums = [0,1,2,3,4]
    var nums = [0,0,1,1,1,2,2,3,3,4]
    var k = removeDuplicates(nums)
    assert.equal(expectedNums.length, k)
    for (let i = 0; i < k; i++) {
        assert.equal(expectedNums[i], nums[i]);
    }
})
