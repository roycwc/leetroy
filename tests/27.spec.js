const { assert } = require('chai')
const { removeElement } = require('../src/27');

it('case 1', () => {
    var expectedNums = [2,2]
    var nums = [3,2,2,3]
    var val = 3
    var k = removeElement(nums, val)
    var expectedLength = expectedNums.length
    var nums2 = nums.slice(0, expectedLength);
    var nums3 = nums2.sort()
    for (let i = 0; i < expectedLength; i++) {
        assert.equal(expectedNums[i], nums3[i])
    }
    assert.equal(2, k)
})

it('case 2', () => {
    var expectedNums = [0,0,1,3,4]
    var nums = [0,1,2,2,3,0,4,2]
    var val = 2
    var k = removeElement(nums, val)
    var expectedLength = expectedNums.length
    var nums2 = nums.slice(0, expectedLength);
    var nums3 = nums2.sort()
    for (let i = 0; i < expectedLength; i++) {
        assert.equal(expectedNums[i], nums3[i])
    }
    assert.equal(5, k)
})
