const { assert } = require('chai')
const { strStr } = require('../src/28');

it('case 1', () => {
    var haystack = "sadbutsad"
    var needle = "sad"
    var k = strStr(haystack, needle)
    assert.equal(0, k)
})

it('case 2', () => {
    var haystack = "leetcode"
    var needle = "leeto"
    var k = strStr(haystack, needle)
    assert.equal(-1, k)
})
