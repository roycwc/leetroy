const { assert } = require('chai')
const { wordPattern } = require('../src/290');

it('case 1', () => {
    var pattern  = "abba"
    var s = "dog cat cat dog"
    var k = wordPattern(pattern, s)
    assert.equal(true, k)
})

it('case 2', () => {
    var pattern  = "abba"
    var s = "dog cat cat fish"
    var k = wordPattern(pattern, s)
    assert.equal(false, k)
})

it('case 3', () => {
    var pattern  = "aaaa"
    var s = "dog cat cat dog"
    var k = wordPattern(pattern, s)
    assert.equal(false, k)
})


it('case 4', () => {
    var pattern  = "abba"
    var s = "dog dog dog dog"
    var k = wordPattern(pattern, s)
    assert.equal(false, k)
})


it('case 5', () => {
    var pattern  = "jquery"
    var s = "jquery"
    var k = wordPattern(pattern, s)
    assert.equal(false, k)
})


it('case 6', () => {
    var pattern  = "abba"
    var s = "dog constructor constructor dog"
    var k = wordPattern(pattern, s)
    assert.equal(true, k)
})
