const { assert } = require('chai')
const { bfs } = require('../common/bfs.js')
const { searchInsert } = require('../src/35');

it('case 0', () => {
    var nums  = [1,2,3,4,5,6,7,8,9,10]
    var target  = 4
    var k = searchInsert(nums, target)
    assert.equal(3, k)
})


it('case 1', () => {
    var nums  = [1,3,5,6]
    var target  = 5
    var k = searchInsert(nums, target)
    assert.equal(2, k)
})


it('case 2', () => {
    var nums  = [1,3,5,6]
    var target  = 2
    var k = searchInsert(nums, target)
    assert.equal(1, k)
})

it('case 3 ', () => {
    var nums  = [1,3,5,6]
    var target  = 7
    var k = searchInsert(nums, target)
    assert.equal(4, k)
})


it('case 4', () => {
    var nums  = [1,3,5,6]
    var target  = 0
    var k = searchInsert(nums, target)
    assert.equal(0, k)
})


it('case 5', () => {
    var nums  = [1,3]
    var target  = 1
    var k = searchInsert(nums, target)
    assert.equal(0, k)
})


it('case 5', () => {
    var nums  = [1,3,5]
    var target  = 4
    var k = searchInsert(nums, target)
    assert.equal(2, k)
})