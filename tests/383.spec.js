const { assert } = require('chai')
const { canConstruct } = require('../src/383');

it('case 1', () => {
    var ransomNote = "a"
    var magazine = "b"
    var k = canConstruct(ransomNote, magazine)
    assert.equal(false, k)
})

it('case 2', () => {
    var ransomNote = "aa"
    var magazine = "ab"
    var k = canConstruct(ransomNote, magazine)
    assert.equal(false, k)
})

it('case 3', () => {
    var ransomNote = "aa"
    var magazine = "aab"
    var k = canConstruct(ransomNote, magazine)
    assert.equal(true, k)
})
