const { assert } = require('chai')
const { isSubsequence } = require('../src/392');

it('case 1', () => {
    var s = "abc"
    var t = "kahbgdc"
    var k = isSubsequence(s, t)
    assert.equal(true, k)
})

it('case 2', () => {
    var s = "axc"
    var t = "ahbgdc"
    var k = isSubsequence(s, t)
    assert.equal(false, k)
})