const { assert } = require('chai')
const { toTreeNode } = require('../common/toTreeNode.js')
const { getMinimumDifference } = require('../src/530');

it('case 1', () => {
    var nums  = [4,2,6,1,3]
    var root = toTreeNode(nums)
    var result = getMinimumDifference(root)
    assert.equal(1, result)
})

it('case 2', () => {
    var nums  = [1,0,48,null,null,12,49]
    var root = toTreeNode(nums)
    var result = getMinimumDifference(root)
    assert.equal(1, result)
})
