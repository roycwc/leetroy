const { assert } = require('chai')
const { lengthOfLastWord } = require('../src/58');

it('case 1', () => {
    var s = "Hello World"
    var k = lengthOfLastWord(s)
    assert.equal(5, k)
})


it('case 2', () => {
    var s = "   fly me   to   the moon  "
    var k = lengthOfLastWord(s)
    assert.equal(4, k)
})


it('case 3', () => {
    var s = "luffy is still joyboy"
    var k = lengthOfLastWord(s)
    assert.equal(6, k)
})
