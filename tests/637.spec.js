const { assert } = require('chai')
const { toTreeNode } = require('../common/toTreeNode.js')
const { averageOfLevels } = require('../src/637');

it('case 1', () => {
    var nums  = [3,9,20,null,null,15,7]
    var root = toTreeNode(nums)
    var result = averageOfLevels(root)
    assert.deepEqual([3.00000,14.50000,11.00000], result)
})


it('case 2', () => {
    var nums  = [3,9,20,15,7]
    var root = toTreeNode(nums)
    var result = averageOfLevels(root)
    assert.deepEqual([3.00000,14.50000,11.00000], result)
})