const { assert } = require('chai')
const { addBinary } = require('../src/67');

it('case 1', () => {
    var a  = "11"
    var b  = "1"
    var k = addBinary(a, b)
    assert.equal("100", k)
})

it('case 2', () => {
    var a  = "1010"
    var b  = "1011"
    var k = addBinary(a, b)
    assert.equal("10101", k)
})
