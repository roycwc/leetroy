const { assert } = require('chai')
const { merge } = require('../src/88');

it('case 1', () => {
    var nums1 = [1, 2, 3, 0, 0, 0]
    var m = 3
    var nums2 = [2, 5, 6]
    var n = 3
    merge(nums1, m, nums2, n)
    assert.deepEqual([1, 2, 2, 3, 5, 6], nums1)
})

it('case 2', () => {
    var nums1 = [1]
    var m = 1
    var nums2 = []
    var n = 0
    merge(nums1, m, nums2, n)
    assert.deepEqual([1], nums1)
})

it('case 3', () => {
    var nums1 = [0]
    var m = 0
    var nums2 = [1]
    var n = 1
    merge(nums1, m, nums2, n)
    assert.deepEqual([1], nums1)
})